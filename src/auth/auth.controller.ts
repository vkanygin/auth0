import { Controller, Get, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';

@Controller('auth0')
@ApiTags('Auth0')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({
    summary: 'login user',
  })
  @Get('/login')
  async loginUser(@Query('code') code: string): Promise<string> {
    console.log(11111);
    return this.authService.loginUser(code);
  }
}
// https://dashboard-v1.whatabyte.app/home, http://localhost:3003/api/auth0/login, http://localhost:3000/api/auth0/login,
/*
export AUTH0_DOMAIN=minurovi.eu.auth0.com
export AUTH0_CLIENT_ID=HD1gS3So7NApALlaR4pGRewmMV1dgBKT
export AUTH0_CLIENT_SECRET=TYDBco1DfAq9hVH4Qv6LP640eeU4aV_vaZ45bgnDgtOs6lErJlyenel-0iJcV1Sq
export CODE=XpFzoNIBpzASvucW06bWDv5ZA1jXdMEUXgoKLLCdfh2IZ

curl -X POST -H 'content-type: application/json' -d '{
"grant_type": "authorization_code",
  "client_id": "'$AUTH0_CLIENT_ID'",
  "client_secret": "'$AUTH0_CLIENT_SECRET'",
  "code": "'$CODE'",
  "redirect_uri": "http://localhost:3003/api/auth0/login"
}' https://$AUTH0_DOMAIN/oauth/token*/
