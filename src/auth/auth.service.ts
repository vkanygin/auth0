import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class AuthService {
  constructor() {}

  async loginUser(code: string) {
    try {
      const res = await axios.post(
        'https://minurovi.eu.auth0.com/oauth/token',
        {
          grant_type: 'authorization_code',
          client_id: 'HD1gS3So7NApALlaR4pGRewmMV1dgBKT',
          client_secret:
            'TYDBco1DfAq9hVH4Qv6LP640eeU4aV_vaZ45bgnDgtOs6lErJlyenel-0iJcV1Sq',
          code: code,
          redirect_uri: 'http://localhost:3003/api/auth0/login',
        },
      );
      console.log(res.data);
      const { access_token: accessToken } = res.data;
      return res.data;
    } catch (error) {
      throw error;
    }
  }
}
